import { Component, OnInit, Input } from '@angular/core';

import { FormGroup, FormControl, Validators} from '@angular/forms';

@Component({
    selector: 'app-page-login',
    templateUrl: 'login.component.html',
    styleUrls: [
        'login.component.scss'
    ]
})

export class LoginPageComponent implements OnInit {
    loginForm: FormGroup;
    email: FormControl;
    password: FormControl;
    constructor() {
        this.buildForm();
    }

    ngOnInit() {
    }

    buildForm() {
        this.loginForm = new FormGroup({
            email: this.email = new FormControl('', [
                Validators.required,
                Validators.email
            ]),
            password: this.password = new FormControl('', [
                Validators.required,
                Validators.minLength(8)
            ])
        });
    }

    onSubmit() {
        
    }

}
