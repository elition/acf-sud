import Question from './questions.model';

class Quizz {
    id: number;
    title: string;
    questions: Question[];
    type: string;
    formationId: number;

    constructor(
        title?,
        questions?,
        id?,
        type?,
        formationId?
    ) {
        this.title = title || '';
        this.questions = questions || [];
        this.id = id || null;
        this.type = type || '';
        this.formationId = formationId || 1;
    }
}

export default Quizz;
