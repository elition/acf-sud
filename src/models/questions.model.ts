class Question {
    public id: number;

    public position: number;
    public title: string;
    public type: string;
    public response: any;

    public options: string[];
    public stringType: string;
    public bareme: number;
    public fileType: string;
    public fileMaxSize: number;

    constructor(
        position?,
        title?,
        type?,
        response?,
        options?,
        bareme?,
        stringType?,
        fileType?,
        fileMaxSize?,
        id?,
    ) {
        this.position = position || null;
        this.title = title || '';
        this.type = type || 'string';
        this.response = response || '';
        this.options = options || [''];
        this.bareme = bareme || 20;
        this.stringType = stringType || 'short';
        this.fileType = fileType || 'pdf';
        this.fileMaxSize = fileMaxSize || 100;
        this.id = id || null;

    }
}

export default Question;