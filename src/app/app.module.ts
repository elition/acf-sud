import { NgModule, PLATFORM_ID, APP_ID, Inject } from '@angular/core';
import { CommonModule, isPlatformBrowser } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AngularSplitModule } from 'angular-split';


import { AppRoutingModule } from './app-routing.module';

import { AppMaterialModule } from './app-material.module';

import { AppComponent } from './app.component';

import { HomePageComponent } from '../pages/home/home.component';
import { DashboardPageComponent } from '../pages/dashboard/dashboard.component';
import { LoginPageComponent } from '../pages/login/login.component';

import { QuizzFormComponent } from '../components/quizz/quizz-form/quizz-form.component';
import { QuizzRenderComponent } from '../components/quizz/quizz-render/quizz-render.component';
import { QuizzEditableComponent } from '../components/quizz/quizz-editable/quizz-editable.component';

import { SplitViewComponent } from '../components/split-view/split-view.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    DashboardPageComponent,
    LoginPageComponent,

    QuizzFormComponent,
    QuizzRenderComponent,
    QuizzEditableComponent,

    SplitViewComponent

  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'acf-sud' }),
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AppMaterialModule,
    AngularSplitModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string
  ) {
    const platform = isPlatformBrowser(platformId)
      ? 'on the server'
      : 'in the browser';
    console.log(`Running ${platform} with appId=${appId}`);
  }
}
