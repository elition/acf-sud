import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';

import { DashboardPageComponent } from '../pages/dashboard/dashboard.component';
import { HomePageComponent } from '../pages/home/home.component';
import { LoginPageComponent } from '../pages/login/login.component';

const appRoutes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomePageComponent },
    { path: 'dashboard', component: DashboardPageComponent },
    { path: 'login', component: LoginPageComponent },
    { path: '**', component: HomePageComponent },
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes
        )
    ],
    exports: [
        RouterModule
    ],
})
export class AppRoutingModule { }
