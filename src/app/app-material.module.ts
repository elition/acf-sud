import { NgModule } from '@angular/core';

import { FlexLayoutModule } from '@angular/flex-layout';

import {
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatToolbarModule,
    MatToolbar,
    MatCardModule,
    MatInputModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule,
    MatSliderModule
} from '@angular/material';

import {
    CovalentLayoutModule,
    CovalentFileModule
} from '@covalent/core';


@NgModule({
    imports: [
        FlexLayoutModule,

        MatButtonModule,
        MatIconModule,
        MatListModule,
        MatToolbarModule,
        MatCardModule,
        MatInputModule,
        MatSelectModule,
        MatAutocompleteModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatExpansionModule,
        MatSliderModule,


        CovalentLayoutModule,
        CovalentFileModule,



    ],
    exports: [
        FlexLayoutModule,

        MatButtonModule,
        MatIconModule,
        MatListModule,
        MatToolbarModule,
        MatCardModule,
        MatInputModule,
        MatSelectModule,
        MatAutocompleteModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatExpansionModule,
        MatSliderModule,

        CovalentLayoutModule,
        CovalentFileModule,


    ],

})
export class AppMaterialModule { }
