import { Component, OnInit, Input, SimpleChange } from '@angular/core';

import { FormGroup, FormControl, Validators} from '@angular/forms';

import Quizz from '../../../models/quizz.model';
import Question from '../../../models/questions.model';


@Component({
    selector: 'app-component-quizz-render',
    templateUrl: 'quizz-render.component.html',
    styleUrls: [
        'quizz-render.component.scss'
    ]
})

export class QuizzRenderComponent implements OnInit {

    @Input() quizz: Quizz;
    @Input() isEditable: boolean = false;

    constructor() {
    }

    ngOnInit() {
    }


    handleUpButton(question: Question) {
        this.quizz.questions[question.position - 1] = this.quizz.questions[question.position - 2];
        this.quizz.questions[question.position - 2] = question;
        this.resetQuestionsPosition();
    }

    handleDownButton(question: Question) {
        this.quizz.questions[question.position - 1] = this.quizz.questions[question.position];
        this.quizz.questions[question.position] = question;
        this.resetQuestionsPosition();
    }

    handleDeleteButton(question: Question) {
        this.quizz.questions = this.quizz.questions.filter(item => item.position !== question.position);
        this.resetQuestionsPosition();
    }

    resetQuestionsPosition() {
        this.quizz.questions.forEach((question, i ) => {
            question.position = i + 1;
        });
    }

}
