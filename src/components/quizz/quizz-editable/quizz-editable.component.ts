import { Component, OnInit, Input } from '@angular/core';

import { FormGroup, FormControl, Validators} from '@angular/forms';

import Quizz from '../../../models/quizz.model';
@Component({
    selector: 'app-component-quizz-editable',
    templateUrl: 'quizz-editable.component.html',
    styleUrls: [
        'quizz-editable.component.scss'
    ]
})

export class QuizzEditableComponent implements OnInit {

    public quizz: Quizz = new Quizz();
    public isRendered: boolean = false;
    public isEditable: boolean = false;

    public primaryAreaSize: number = 50;
    public secondaryAreaSize: number = 50;
    public prevSize: any = [this.primaryAreaSize, this.secondaryAreaSize];

    constructor() {
    }

    ngOnInit() {
    }

    handleRenderButton() {
        this.isRendered = !this.isRendered;
    }

    handleEditButton() {
        this.isEditable = !this.isEditable;
    }

    handleSubmitButton() {
        console.log(this.quizz);
    }
}
