import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-component-split-view',
    templateUrl: 'split-view.component.html',
    styleUrls: [
        'split-view.component.scss'
    ]
})

export class SplitViewComponent implements OnInit {

    @Input() data: any;
    @Input() primaryComponent: Component;
    @Input() secondaryComponent: Component;
    
    constructor() { }

    ngOnInit() { }
}